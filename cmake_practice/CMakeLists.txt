CMAKE_MINIMUM_REQUIRED ( VERSION 2.4 )

PROJECT("cmake_practice")

##실행파일##############################################################################################
SET(SRC_DIR main.cpp)
SET(APP_NAME app)


ADD_EXECUTABLE(${APP_NAME} ${SRC_DIR})



LINK_DIRECTORIES ( /home/david/Desktop/moderncpp/cmake_practice )
#UNIT_LIB 변수에다가 paths에 있는 custom의 라이브러리를 찾아서 담아준다!
FIND_LIBRARY(UNIT_LIB NAMES libcustom PATHS ./cmake_practice )
TARGET_LINK_LIBRARIES( ${APP_NAME} ${UNIT_LIB} )


##라이브러리#############################################################################################

#SET(SRC_DIR lib.cpp) 
#SET(LIB_NAME custom)

#ADD_LIBRARY(${LIB_NAME} ${SRC_DIR})