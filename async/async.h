#include <future>
#include <iostream>
#include <thread>

int testcount = 0;

void testPrinter(std::string str)
{
    for(int i = 0; i < 10; ++i)
    {
        std::cout<<testcount<<str<<std::endl;
        ++testcount;
    }
}

//람다식으로 구현했다고 하더라도 병렬화 되는건 아니다.
void futureSequenceTest()
{
    std::promise<int> prom;
    std::future<int> fut = prom.get_future();

    std::cout<<"1"<<std::endl;

    std::thread thread1([&prom](){
        testPrinter("future_test");
        prom.set_value(1000);
        std::cout<<"3"<<std::endl;
    });
    //std::cout<<"2"<<std::endl;
    thread1.join();
    //std::cout<<"4"<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
    //std::cout<<fut.get()<<std::endl;
}


int func()
{
    testPrinter("func");
    return 1000;
}

void asyncSequenceTest()
{
    std::cout<<"1"<<std::endl;
    auto fut = std::async(std::launch::async, func);
    std::cout<<"2"<<std::endl;
    testPrinter("main");
    //get을 기준으로 락이 걸린다.
    int result = fut.get();
    std::cout<<"3"<<std::endl;
}