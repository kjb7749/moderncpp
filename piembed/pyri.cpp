#include <iostream>
#include <python2.7/Python.h>
#include <fcntl.h>

using namespace std;
#define BUF_SIZE 2048


int main(int argc, char * argv[])
{
    cout<<"it's for python embedding and gpio com"<<endl;
    int fd = open("../testpy.py", O_RDONLY);

    if(fd == -1)
        return 1;

    char * buf = new char[BUF_SIZE];
    memset(buf, 0, BUF_SIZE);
    read(fd, buf, BUF_SIZE);
    close(fd);


    PyObject *pName, *pModule, *pFunc;
    PyObject *pArgs, *pValue;

    Py_Initialize();                    //Python Embdding을 위한 초기화 설정

    pName = PyString_FromString(buf);
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    if (pModule != NULL) {
        Py_DECREF(pModule);
    }

    PyRun_SimpleString(buf);  //Python 코드를 바탕으로 현재 시간 출력
    Py_Finalize();                      //Python Embdding 종료
    return 0;
}