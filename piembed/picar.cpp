#include <iostream>
#include <python2.7/Python.h>
#include <boost/python.hpp>

using namespace boost::python;

int main( int argc, char ** argv ) {
  try {
    Py_Initialize();

    object main_module((
      handle<>(borrowed(PyImport_AddModule("__main__")))));

    object main_namespace = main_module.attr("__dict__");

    handle<> ignored(( PyRun_String( "print \"Hello, World\"",
                                     Py_file_input,
                                     main_namespace.ptr(),
                                     main_namespace.ptr() ) ));
  } catch( error_already_set ) {
    PyErr_Print();
  }

  return 0;
}

// int main(int argc, char *argv[])
// {
//     Py_Initialize(); //Python Embdding을 위한 초기화 설정

//     object main_module((
//         handle<>(borrowed(PyImport_AddModule("__main__")))));

//     object main_namespace = main_module.attr("__dict__");

//     handle<> ignored((PyRun_String("print \"Hello, World\"",
//                                    Py_file_input,
//                                    main_namespace.ptr(),
//                                    main_namespace.ptr())));

//     Py_Finalize(); //Python Embdding 종료
//     return 0;
// }

    /*PyObject * servo_test = PyImport_ImportModule("scripts.servo_test");
    cout<<"1"<<endl;

    if(servo_test)
    {
            cout<<"2"<<endl;
        PyObject * setup = PyObject_GetAttrString(servo_test, "setup");
                    cout<<"3"<<endl;
        PyObject *result = PyObject_CallFunction(setup, NULL);

        if(result)
        {
            Py_XDECREF(result); 
        }
        Py_XDECREF(setup);
    }
    Py_XDECREF(servo_test);*/

// def PrintMyDef():
//     print "Hello, MyDef!"

// def Multiply(x, y):
//     return x * y

// class MyClass:
//     def __init__(self):
//         print "MyClass instantiated"

//     def __del__(self):
//         print "MyClass deleted"

//     def Test(self):
//         print "Ok, tested!"

// PyObject* mydef = PyImport_ImportModule("PCA9685.py");
// int result;

// if(mydef) {
//     PyObject* multiply = PyObject_GetAttrString(mydef, "Multiply");

//     if(multiply) {
//     PyObject *r = PyObject_CallFunction(multiply, "ii", x, y);

//     if(r) {
//         //자료형 변환을 위한 함수이다. 자매품으로 floatobject.h의 PyFloat_AS_DOUBLE(r)이 있다.
//         result = (int)PyInt_AS_LONG(r);
//         //필요 없는 파이썬 오브젝트 메모리를 해제하는것으로 추측됨
//         Py_XDECREF(r);
//     }
//     Py_XDECREF(multiply);
//     }
//     Py_XDECREF(mydef);
// }

// def setup():
//     global pwm
//     pwm = servo.PWM()

// def servo_test():
//     for value in range(MinPulse, MaxPulse):
//         pwm.write(0, 0, value)
//         pwm.write(14, 0, value)
//         pwm.write(15, 0, value)
//         time.sleep(0.002)

// if __name__ == '__main__':
//     setup()
//     servo_test()