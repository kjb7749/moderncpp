#include <iostream>
#include <thread>

using namespace std;


void t_func();
void t_funcStr(const char *str);

int main(int argc, char* argv[])
{
    //thread thread1(t_func);
    //thread thread2(t_funcStr, "hihi");

    //thread1.join();
    //thread2.join();

    thread lambdaThread([]() {
		while (true)
		{
			cout << "sub" << endl;
		}
	});
   	lambdaThread.join();

    while(true)
    {
        cout<< "main" <<endl;
    }

    return 0;
}

void t_func()
{
    for(int i = 0; i < 100000; i+=2)
    {
        cout<<i<<endl;
    }
}

void t_funcStr(const char *str)
{
    cout<<str<<endl;
    for(int i = 1; i < 100000; i+=2)
    {
        cout<<i<<endl;
    }
}



